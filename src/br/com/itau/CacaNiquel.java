package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class CacaNiquel {

    int quantidadeSlot;

    public CacaNiquel(int quantidadeSlot) {
        this.quantidadeSlot = quantidadeSlot;
    }

    public List<Simbolos> jogar() {

        List<Simbolos> simbolosSorteados = new ArrayList<>();

        for (int i = 0; i < this.quantidadeSlot; i++) {
            Sorteio sorteio = new Sorteio();
            //sorteio.buscar();
            simbolosSorteados.add(sorteio.buscar());
        }
        return simbolosSorteados;
    }

    public void exibirResultado(List<Simbolos> simbolos) {

        Integer pontuacao = 0;
        Simbolos simboloAnterior = null;
        Integer quantidadeSimbolosIguais = 0;

        for (Simbolos imgagem : simbolos) {
            pontuacao += imgagem.getValor();

            if (simboloAnterior == null || imgagem == simboloAnterior) {
                quantidadeSimbolosIguais++;
                simboloAnterior = imgagem;
            }
        }
        if(simbolos.size() == quantidadeSimbolosIguais)
        {
            pontuacao = pontuacao * 100;
        }

        Imprime.totalPontuacao(pontuacao);
    }
}