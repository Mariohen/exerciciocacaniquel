package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Imprime {

    public static Map<String, String> solicitarNome(){
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Entre com seu nome : ");
        String nome = scanner.nextLine();

        System.out.printf("Entre com sua idade : ");
        String idade = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("idade", idade);

        return dados;
    }

    public static void totalPontuacao (Integer pontuacao){
        System.out.println( " = " + pontuacao);
    }

}
