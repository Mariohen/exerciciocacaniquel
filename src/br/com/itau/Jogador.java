package br.com.itau;

public class Jogador {
    String nome;
    Integer idade;

    public Jogador(String nome, Integer idade) {
        this.nome = nome;
        this.idade = idade;

        if (this.idade < 18){
            throw new ArithmeticException("Jogo não permitido para menor de idade");
        }


    }


}
