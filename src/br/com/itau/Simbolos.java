package br.com.itau;

public enum Simbolos {
    Banana(10),
    Framboesa(50),
    Moeda(100),
    Sete(300);

    private int valor;

    public int getValor(){
        return valor;
    }
    Simbolos (int valor){
        this.valor = valor;
    }

}
