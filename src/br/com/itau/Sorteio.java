package br.com.itau;

import java.util.Random;

public class Sorteio {

    public Simbolos buscar(){
        Random random = new Random();
        Integer numeroSorteado = random.nextInt(Simbolos.values().length);
        System.out.print ("| " + Simbolos.values()[numeroSorteado] + " | ");
        return Simbolos.values()[numeroSorteado];
     }
}
